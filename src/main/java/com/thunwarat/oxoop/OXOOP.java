/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.oxoop;

/**
 *
 * @author ACER
 */
public class OXOOP {

    public static void main(String[] arge) {
        Game game = new Game();
        game.newBoard();
        game.showWelcome();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showStat();
                game.newBoard();
            }
        }
    }
}
