/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.oxoop;

/**
 *
 * @author ACER
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    private int row;
    private int col;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public boolean setRowCol(int row, int col) {
        this.row = row;
        this.col = col;

        if (row > 3 || col > 3 || row < 1 || col < 1 || table[row - 1][col - 1] != '-') {
            return false;
        }
        count++;
        table[row - 1][col - 1] = currentPlayer.getSymbol();
         if (checkWin()) {
            updateStatWinner();
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            updateStatDraw();
            this.draw = true;
            return true;
        }

        switchPlayer();
        return true;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }
    private void updateStatDraw() {
        o.draw();
        x.draw();
    }

    private void updateStatWinner() {
        if (this.currentPlayer == o) {
            o.win();
            x.loss();
        } else {
            x.win();
            o.loss();
        }
    }

    public boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }
        return false;
    }

    public boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDraw() {
        if (this.count == 9) {
            return true;
        }
        return false;
    }

}
